import React, { Component } from "react";
import './App.css';
import Header from "./components/layout/Header";
import Weatherdetail from "./components/WeatherDetail.js";
import Weekdays from './components/Weekdays';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: {},
      weatherReport: [],
      selectedDayReport: [],
      loading: true,
      locationAccessError: false,
    }
  }

  componentDidMount() {
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        let lat = position.coords.latitude;
        let lon = position.coords.longitude;
        let api = `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=400bf1de7408fdd721fecbbab3d5c084`;
        fetch(api)
        .then((resp) => {
          this.hideLoading();
          return resp.json();
        })
        .then((data) => {
          let { city , list:weatherReport } = data;
          let structuredReport = this.getStructuredReport(weatherReport);
          if(structuredReport !== undefined) {
            this.setState({
              city,
              weatherReport: structuredReport,
              selectedDayReport: structuredReport[0].report,
            });
          }
        })
        .catch((err) => {
          console.log(err.message);
        });
      },
      (err) => {
        if (err.code === err.PERMISSION_DENIED) {
          this.hideLoading();
          this.setState({
          locationAccessError: true
          })
        } 
      });
    } 
  }

  getDay(dt) {
    let date = new Date(dt * 1000);
    let reportDay = date.getDay();
    return reportDay;
  }

  getStructuredReport(weatherReport) {
    let dayArray = weatherReport.map((report) => {
      let { dt } = report;
      let reportDay = this.getDay(dt);
      return reportDay;
    });

    dayArray = [...new Set(dayArray)];

    let structuredWeatherReport = dayArray.map((day,index) => {
      let filteredReport = weatherReport.filter((report) => {
        let { dt } = report;
        let reportDay = this.getDay(dt);
        if(reportDay === day) {
          return true;
        } 
        return false;
      });
      if(index === 0){
        return {
          day,
          report: filteredReport,
          showDetails: true
        }
      }
      return {
        day: day,
        report: filteredReport,
        showDetails: false
      }
    });

    return structuredWeatherReport;
  }

  hideLoading() {
    this.setState({
      loading: false
    })
  }


  handleClick = (day) => {
    this.setState({ weatherReport: this.state.weatherReport.map(reportObj => {
      if(reportObj.day === day && reportObj.report !== undefined) {
        reportObj.showDetails = true;
      } else {
        reportObj.showDetails = false;
      }
      return reportObj;
    }) });
    let filteredReport = this.state.weatherReport.filter((reportObj) => {
        if(reportObj.day === day && reportObj.report !== undefined){
          return true;
        }
        return false;
      });
    this.setState({
      selectedDayReport: filteredReport[0].report
    });
  }

  render() {
    return (
      <div className='App'>
        <Header city={this.state.city} />
        {
          this.state.loading && 
          <div className='Loading'>
            <div className='Spinner'></div>
            <p className='LoadingText'>Loading</p>
          </div>
        }
        {
          this.state.locationAccessError &&
          <div className="Errorwrapper">
            <p className="Error">Allow location access for weather forecast</p>
          </div>
        }
        <Weekdays weatherReport={this.state.weatherReport} handleClick={this.handleClick} />
        <Weatherdetail  report={this.state.selectedDayReport} />
      </div>
    );
  }
}

export default App;
