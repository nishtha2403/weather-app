import React, { Component } from 'react';
import locationImg from '../../assets/location.png';

export class Header extends Component {
    render() {
        let { name:locationName } = this.props.city;
        return (
            <div style={headerStyle}>
                <h2 style={ titleStyle }>Forecast report</h2>
                <div style={locationWrapper}>
                <img src={locationImg}  style={locationIcon} alt="Not available" />
                <h3>{ locationName }</h3>
                </div>
            </div>
        )
    }
}

const headerStyle = {
    padding: '15px',
    textAlign: 'center',
    letterSpacing: '1.5px',
    color: 'white',
    marginBottom: '50px',
}
const titleStyle = {
    fontFamily: 'cursive',
    letterSpacing: '3px',
    marginBottom: '10px',
    fontSize: '40px'
}
const locationWrapper = {
    display: 'flex',
    justifyContent: 'center'
}
const locationIcon = {
    width: '25px',
    height: '25px',
    marginRight: '5px'
}

export default Header