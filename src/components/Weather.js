import React, { Component } from 'react';
import './Weather.css';
export class Weather extends Component {
    constructor(props){
        super(props);
        this.state = {
            showDetails: false
        }
    }

    componentDidMount() {
        this.setState({
            showDetails: this.props.showDetails
        })
    }

    getDayName = (day) => {
        let shortDayNameArray = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        return shortDayNameArray[day];
    }

    getIconUrl = (icon) => {
        return `https://openweathermap.org/img/w/${icon}.png`;
    }

    getTemp = (temp) => {
        return Math.floor(temp - 273.15);
    }

    render() {
        let firstReport = this.props.report[0];
        let showDetails = this.props.showDetails;
        let day = this.props.day;
        let { icon } = firstReport.weather[0];
        let { temp } = firstReport.main;
        let dayName = this.getDayName(day);
        let iconUrl = this.getIconUrl(icon);
        temp = this.getTemp(temp);
        return (
            <div>
                <div className={showDetails?"WeatherStyle2":"WeatherStyle1"} 
                onClick={this.props.handleClick.bind(this,day)} >
                    <p className="HeadingStyle">{ dayName }</p>
                    <img src={ iconUrl } className="ImgStyle" alt="Not available"/>
                    <p className="DescStyle">{ temp } °C</p>
                </div>
            </div>
        )
    }
}

export default Weather
