import React, { Component } from 'react';
import './WeatherDetail.css';

export class Weatherdetail extends Component {
    getCurrTime(dt) {
        let date = new Date(dt * 1000);
        let hrs = date.getHours();
        let mins = date.getMinutes();
        let reportTime = `${hrs}:${mins}`;
        return reportTime;
    }

    getDayName = (dt) => {
        let date = new Date(dt * 1000);
        let day = date.getDay();
        let shortDayNameArray = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        return shortDayNameArray[day];
    }

    getIconUrl = (icon) => {
        return `https://openweathermap.org/img/w/${icon}.png`;
    }

    getTemp = (temp) => {
        return Math.floor(temp - 273.15);
    }

    render() {
        let reportAllTimes = this.props.report;
        if(reportAllTimes !== undefined) {
            reportAllTimes = reportAllTimes.map((comingReport) => {
                let { dt } = comingReport;
                let { icon } = comingReport.weather[0];
                let { temp } = comingReport.main; 
                temp = this.getTemp(temp);
                let iconUrl = this.getIconUrl(icon);
                let time = this.getCurrTime(dt);
                let dayName = this.getDayName(dt);
                let comingReportObject = {
                    dt,
                    dayName,
                    time,
                    iconUrl,
                    temp
                }
    
                return comingReportObject;
            });
            return (
                <div>
                    <div className="detailWrapperStyle">
                        {
                            reportAllTimes.map((comingReport) => (
                                <div className="Weatherdetailstyle" key={comingReport.dt}>
                                    <p className="Day">{comingReport.dayName}</p>
                                    <p className="Header">{comingReport.time}</p>
                                    <img src={ comingReport.iconUrl } className="Img" alt="Not available"/>
                                    <p className="Desc">{comingReport.temp} °C</p>
                                </div>
                            ))
                        }
                    </div>
                </div>
            )
        }
    }
}

export default Weatherdetail
