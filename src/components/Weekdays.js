import React, { Component } from 'react';
import Weather from './Weather';

export class Weekdays extends Component {

    render() {
        return (
            <div style={weekdayStyle}>
                {
                    this.props.weatherReport.map((reportObject) => (
                        <Weather 
                        key={reportObject.day} 
                        report={reportObject.report} 
                        day={reportObject.day} 
                        showDetails={reportObject.showDetails}
                        handleClick={this.props.handleClick}
                        />
                    ))
                }
            </div>
        )
    }
}

const weekdayStyle = {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap"
}

export default Weekdays
